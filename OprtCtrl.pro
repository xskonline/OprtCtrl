QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = OprtCtrl
TEMPLATE = app

SOURCES += main.cpp\
        widget.cpp \
    encode.cpp \
    bitstream.c \
    mask.c \
    mmask.c \
    mqrspec.c \
    qrenc.c \
    qrencode.c \
    qrinput.c \
    qrspec.c \
    rscode.c \
    split.c

HEADERS  += widget.h \
    bitstream.h \
    config.h \
    encode.h \
    mask.h \
    mmask.h \
    mqrspec.h \
    qrencode.h \
    qrencode_inner.h \
    qrinput.h \
    qrspec.h \
    rscode.h \
    split.h

FORMS    += widget.ui
RC_FILE = ico.rc

RESOURCES += \
    res.qrc
