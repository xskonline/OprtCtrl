#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QThread>
#include <QCursor>
#include <QScreen>
#include <QSize>
#include <QDesktopWidget>
#include <QtNetwork>
#include <QMessageBox>
#include <QSystemTrayIcon>
#include <windows.h>

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

    // 鼠标移动
    void moveCursor(QCursor *cur, int xAcc, int yAcc);
    // 模拟按键
    void sendKey(int keys[], int n);
    // 模拟鼠标
    void mouseCtrl(int type);

    // 本地网络绑定配置
    int udpLocalPort;
    QHostAddress *localAddr;
    QString getIP();

private:
    Ui::Widget *ui;
    QUdpSocket *udpSocket;
    QSystemTrayIcon *tray;
    // 屏幕高宽
    QDesktopWidget dw;
    QRect mainScreenSize = dw.screenGeometry(dw.primaryScreen());//C++11
    const int H = mainScreenSize.height() - 1, W = mainScreenSize.width() - 1;//C++11
    // 显示二维码
    void showQR();

private slots:
    // 接受信息
    void receive();
};

#endif // WIDGET_H
