#include "qrencode.h"
#include <string.h>
#include <errno.h>
#include <conio.h>
#include <ctype.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <wchar.h>
#include <qmessagebox.h>
#include <qtextcodec.h>
                  
#define OUT_FILE_PIXEL_PRESCALER	8	//每个QRCode8个像素										// Prescaler (number of pixels in bmp file for each QRCode pixel, on each dimension)

#define PIXEL_COLOR_R				0		//图片颜色									// Color of bmp pixels
#define PIXEL_COLOR_G				0
#define PIXEL_COLOR_B				0xff

typedef unsigned short	WORD;
typedef unsigned long	DWORD;
typedef signed long		LONG;

#define BI_RGB			0L

#pragma pack(push, 2)

//typedef struct //BMP文件头结构体
//{
//    WORD    bfType;
//    DWORD   bfSize;
//    WORD    bfReserved1;
//    WORD    bfReserved2;
//    DWORD   bfOffBits;
//} BITMAPFILEHEADER;

//typedef struct //位图信息结构体
//{
//    DWORD      biSize;
//    LONG       biWidth;
//    LONG       biHeight;
//    WORD       biPlanes;
//    WORD       biBitCount;
//    DWORD      biCompression;
//    DWORD      biSizeImage;
//    LONG       biXPelsPerMeter;
//    LONG       biYPelsPerMeter;
//    DWORD      biClrUsed;
//    DWORD      biClrImportant;
//} BITMAPINFOHEADER;

#pragma pack(pop)
bool encode(const char *qrstring);
