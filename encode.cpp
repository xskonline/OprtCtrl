#include "encode.h"
bool encode(const char *qrstring)
{
    char * OUT_FILE = "test.bmp";
    unsigned int	unWidth, x, y, l, n, unWidthAdjusted, unDataBytes;
    unsigned char*	pRGBData, *pSourceData, *pDestData;
    QRcode*			pQRC;
    FILE*			f;

    if(pQRC = QRcode_encodeString(qrstring, 0, QR_ECLEVEL_H, QR_MODE_8, 1))//生成二维码
       {
       unWidth = pQRC->width;
       unWidthAdjusted = unWidth * OUT_FILE_PIXEL_PRESCALER * 3;
       if (unWidthAdjusted % 4)
           unWidthAdjusted = (unWidthAdjusted / 4 + 1) * 4;
       unDataBytes = unWidthAdjusted * unWidth * OUT_FILE_PIXEL_PRESCALER;

           // Allocate pixels buffer

       if (!(pRGBData = (unsigned char*)malloc(unDataBytes)))//申请图片缓存
           {
           printf("Out of memory");
           exit(-1);
           }

           // Preset to white

       memset(pRGBData, 0xff, unDataBytes);//缓存初始化


//               // Prepare bmp headers

       BITMAPFILEHEADER kFileHeader;//准备bmp文件头
       kFileHeader.bfType = 0x4d42;  // "BM"
       kFileHeader.bfSize =	sizeof(BITMAPFILEHEADER) +
                               sizeof(BITMAPINFOHEADER) +
                               unDataBytes;
       kFileHeader.bfReserved1 = 0;
       kFileHeader.bfReserved2 = 0;
       kFileHeader.bfOffBits =	sizeof(BITMAPFILEHEADER) +
                               sizeof(BITMAPINFOHEADER);

       BITMAPINFOHEADER kInfoHeader;
       kInfoHeader.biSize = sizeof(BITMAPINFOHEADER);
       kInfoHeader.biWidth = unWidth * OUT_FILE_PIXEL_PRESCALER;
       kInfoHeader.biHeight = -((int)unWidth * OUT_FILE_PIXEL_PRESCALER);
       kInfoHeader.biPlanes = 1;
       kInfoHeader.biBitCount = 24;
       kInfoHeader.biCompression = BI_RGB;
       kInfoHeader.biSizeImage = 0;
       kInfoHeader.biXPelsPerMeter = 0;
       kInfoHeader.biYPelsPerMeter = 0;
       kInfoHeader.biClrUsed = 0;
       kInfoHeader.biClrImportant = 0;

       //将二维码转换成bmp图片
       pSourceData = pQRC->data;
       for(y = 0; y < unWidth; y++)
       {
           pDestData = pRGBData + unWidthAdjusted * y * OUT_FILE_PIXEL_PRESCALER;
           for(x = 0; x < unWidth; x++)
           {
               if (*pSourceData & 1)//如果码字为1在图片上绘制图片
               {
               for(l = 0; l < OUT_FILE_PIXEL_PRESCALER; l++)//占用8个像素
                   {
                   for(n = 0; n < OUT_FILE_PIXEL_PRESCALER; n++)//占用8个像素
                       {
                       *(pDestData +		n * 3 + unWidthAdjusted * l) =	PIXEL_COLOR_B;
                       *(pDestData + 1 +	n * 3 + unWidthAdjusted * l) =	PIXEL_COLOR_G;
                       *(pDestData + 2 +	n * 3 + unWidthAdjusted * l) =	PIXEL_COLOR_R;
                       }
                   }
               }
               pDestData += 3 * OUT_FILE_PIXEL_PRESCALER;
               pSourceData++;//获取下一个码字
           }

       }

       if ((f=fopen(OUT_FILE, "wb")))
       {
           fwrite(&kFileHeader, sizeof(BITMAPFILEHEADER), 1, f);//写文件头
           fwrite(&kInfoHeader, sizeof(BITMAPINFOHEADER), 1, f);//写位图信息
           fwrite(pRGBData, sizeof(unsigned char), unDataBytes, f);//写二维码图片
           fclose(f);
           free(pRGBData);
           QRcode_free(pQRC);
           return true;
       }
       free(pRGBData);
       QRcode_free(pQRC);
    }
    return false;
}
